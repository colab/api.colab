"use strict";
/// <reference path="typings/node/node.d.ts"/>

import http = require('http');

var myStatusTree: StatusTree = {
	name: '',
	status: 'unavailable',
	children: [],
	description: 'Status module just booted, no status has been set yet'
};
var lastChange: Date = new Date();

export function init(app: string): void {
	myStatusTree.name = app;
	myStatusTree.description = 'Initialized status module with app name: ' + app;
}

export function setTo(s: string): void {
	switch (s) {//silently fails to update if it's not one of the proper values
		default://this is intentional
			return;
		case 'running':
		case 'issues':
		case 'critical':
		case 'maintainance':
		case 'unavailable':
		case 'failed':
		case 'crashed':
	}
	if (myStatusTree.status != s) {
		myStatusTree.description = "Became " + s + " at " + (new Date().toString()) + ". Changed from " + myStatusTree.status + " which had been the status since " + lastChange.toString();
		lastChange = new Date();
		myStatusTree.status = s;
	}//if no change, then no change
}

export function get(): string {
	return myStatusTree.status;
}

export function getString(): string {
	return JSON.stringify(myStatusTree);
}

export interface StatusTree {
	name: string
	status: string
	children: StatusTree[]
	description: string
}

