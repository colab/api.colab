"use strict";
//Auth Manager used in api.colab 
//Originally developed by Jason "Toolbox" Oettinger
import http = require('http');
import https = require('https');
import querystring = require('querystring');

import admins = require('./admins');
import api = require('./api');
import endpointManager = require('./endpointManager');
import metaAPI = require('./metaAPI');
import storage = require('./storage');
import jsonerror = require('jsonerror');

import debugInit = require('debug');
var CONFIG = require('../config.json');

var debug = debugInit('authManager.api.colab');

debug('Starting module. Required all libraries.');

//-----------
//STATE BELOW
//-----------
var apps: { [index: string]: storage.App } = {};
var scopes: { [index: string]: storage.Scope } = {};
//-----------
//STATE ABOVE
//-----------
updateState();
setInterval(updateState, 25937); //~ every 25 seconds

function updateState() {
  storage.ready()
    .then(() => storage.getApps())
    .then((storedApps: { [index: string]: storage.App }) => {
      apps = storedApps;
      // debug('updated apps', apps);
      if (!('testapi' in apps)) {
        apps['testapi'] = new storage.App({}, 'colab', true);//TODO REMOVE
      }
    })
    .then(storage.getScopes)
    .then((storedScopes: { [index: string]: storage.Scope }) => {
      scopes = storedScopes;
      // debug('updated scopes', scopes);
    });
}


export function auth(req: api.Request): Promise<api.Request | string> {

  //check that the api key is valid
  if (req.appKey && req.appKey in apps) {//if the key exists in the request and in the database
    if (Date.now() > apps[req.appKey].expiration) { return Promise.reject<string>("Expired API Key!"); }//make sure the key's expiration is valid, otherwise kill connection
  } else {
    return Promise.reject<string>("Unregistered API Key!");//key doesn't exist? we should reject! 
  }
  //if oauth isn't required
  if (req.operation.scopes.length === 0) {
    return Promise.resolve(req);//call the callback if we don't need to check oauth
  } else {
    //check that oauth scopes are valid
    if (req.oauthToken) {
      return getAllowedScopes(req)//get the scopes from the oauth server
        .then((allowedScopes): Promise<api.Request | string> => {

          debug("Require scopes: " + req.operation.scopes);
          debug("Allowed scopes: " + allowedScopes);
          for (var i = 0; i < req.operation.scopes.length; i++) {//iterate through every required scope
            if (allowedScopes.indexOf(req.operation.scopes[i]) < 0) {//if any of the required scopes are not in the allowed scopes
              return Promise.reject<string>("This token doesn't have the required scopes!");//you shall not pass
            }
          }

          return Promise.resolve(req);//if every required scope was in the allowed scopes then we're good to go into the callback
        })
        .catch(() => {
          return Promise.reject("Couldn't determine scopes for this token.");
        });
    } else {
      return Promise.reject<string>("No OAuth token sent when one was required.");//well of course let's reject it if there's no token
    }
  }
}


//takes in the api request and the server response object, and a callback that will be called when the result is obtained
//calls limit(res) if errors occur

export function getAllowedScopes(req: api.Request): Promise<string[]> {
  return new Promise(function (resolve, reject) {
    var httpreq: http.ClientRequest = https.request(
      {//modify this to change the resource evaluator url for oauth
        hostname: CONFIG.oauthHost,
        port: 443,
        path: CONFIG.oauthValidationPath + '?access_token=' + req.oauthToken,
        method: 'POST',
        headers: {
          'Connection': 'keep-alive'//keeps the connection alive between here and the oauth server to minimize tcp overhead **hopefully**
        }
      },
      function (result: http.IncomingMessage): void {
        //pull out the resultant scopes
        var data: any = '';
        result.on('data', function (_data) {
          data += _data;
        });
        result.on('end', function () {
          try {
            debug('result from auth: ', data);
            data = JSON.parse(data);
            req.eppn = data.eppn;//add eppn to req for future use
            resolve(data.scope.split(' '));
          } catch (e) {
            reject();
          }
        });
      }
    );
    httpreq.on('error', reject);
    httpreq.end();
  });
}

export function onAddApi(s: any): Promise<any> {

  return Promise.resolve(s);
}

export function onRemoveApi(api): Promise<any> {
  return Promise.resolve(api);//no need to remove scopes
}

export function onAddApp(req: api.Request): Promise<any> {
  //check to see if apps exist already

  var app: storage.App = new storage.App(JSON.parse(req.bodyData), req.eppn);
  debug('Incoming requests', req.bodyData, req.eppn);
  debug("Adding app: " + app.clientId);
  if (!app.isValid()) {
    return Promise.reject("Invalid app input.");
  }
  if (apps[app.clientId]) {
    //check permissions if so
    if (apps[app.clientId].appOwners.indexOf(app.appOwners[0]) < 0 && !admins.contains(app.appOwners[0])) {
      return Promise.reject(app);
    }
    return storage.updateApp(apps[app.clientId], app, req.oauthToken)
      .then(function () {
        //add to apps list
        apps[app.clientId] = app;
        return app;
      });
  }
  return storage.addApp(app, req.oauthToken)
    .then(function () {
      //add to apps list
      apps[app.clientId] = app;
      return app;
    });
}

export function onRemoveApp(req: api.Request): Promise<any> {
  //check to see if apps exist already
  var app: storage.App = new storage.App(JSON.parse(req.bodyData), req.eppn)
  debug("Removing app: " + app.clientId);
  if (!app.isValid()) {
    return Promise.reject("Invalid app input.");
  }
  if (app.clientId in apps) {
    //check permissions if so
    if (apps[app.clientId].appOwners.indexOf(app.appOwners[0]) < 0 && !admins.contains(app.appOwners[0])) {
      return Promise.reject("You don't own that!");
    } else {
      return storage.removeApp(apps[app.clientId], req.oauthToken)
        .then(function () {
          delete apps[app.clientId];
        });
    }
  } else {
    Promise.resolve(app);
  }

}

export function viewMyApps(eppn: string): storage.App[] {
  var myApps: storage.App[] = [];
  for (var i in apps) {
    if (apps[i].appOwners && apps[i].appOwners.indexOf(eppn) > -1) {
      myApps.push(apps[i]);
    }
  }
  return myApps;
}

export function viewAppsAs(eppn: string): storage.App[] {
  var appList: storage.App[] = [];
  for (var i in apps) {
    if (admins.contains(eppn) || (apps[i].appOwners && apps[i].appOwners.indexOf(eppn) > -1)) {
      appList.push(apps[i]);
    } else {
      appList.push(
        new storage.App({ clientId: apps[i].clientId, appOwners: apps[i].appOwners }, apps[i].appOwners ? apps[i].appOwners[0] : '', true)
      );
    }

  }
  return appList;
}
