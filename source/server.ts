"use strict";
//api.colab Server
//Duke API system, originally developed by Jason "Toolbox" Oettinger
var VERSION = require('../package.json').version;
var CONFIG = require('../config.json');
var PROD = true;
for (var i = 0; i < process.argv.length; i++) {
    PROD = PROD && (process.argv[i] !== "debug") && process.env["NODE_ENV"] !== 'development'
}
if (!PROD) { process.env['DEBUG'] = '*'; }

var httpsPort = CONFIG.httpsPort || (PROD ? 443 : 8081);

import debugInit = require('debug');
var debug = debugInit('api.colab');
debug("Debugger started.");
debug("Starting api.colab version: " + VERSION)

import https = require('https');
import http = require('http');
import url = require('url');
import fs = require('fs');
import jsonerror = require('jsonerror');

import rateLimiter = require('./rateLimiter');
import authManager = require("./authManager");
import api = require("./api");
import metaAPI = require("./metaAPI");
import endpointManager = require('./endpointManager');
import status = require('./status');

debug('Starting module. Required all libraries.');

status.init('api.colab');//initialize status monitor with the app's name

//extract intermediates
var ca = [];
if (PROD) {
    var chain = fs.readFileSync(CONFIG.tls.intermediates || 'ssl/intermediates.cert', 'utf8');
    var chainStringArray = chain.split("\n");
    var cert = [];
    for (var l in chainStringArray) {
        var line = chainStringArray[l];
        cert.push(line);
        if (line.match(/-END CERTIFICATE-/)) {
            ca.push(cert.join("\n"));
            cert = [];
        }
    }
}

//MAKES A SERVER. Options are also helpful
var options: https.ServerOptions = {
    key: fs.readFileSync(PROD ? CONFIG.tls.key || 'ssl/api.colab.key' : 'ssl/snakeoil.key'),
    cert: fs.readFileSync(PROD ? CONFIG.tls.cert || 'ssl/api.colab.cert' : 'ssl/snakeoil.cert'),
    ca: ca
};
var server: https.Server = https.createServer(options, onRequest).listen(httpsPort);
debug('Started server on port ' + httpsPort);
status.setTo('running');


var req_id: number = 0;//increment with each request
function onRequest(req: http.IncomingMessage, res: http.ServerResponse) {
    req_id++;
    res.setHeader('Server', 'api.colab/' + VERSION);
    res.setHeader('Content-Type', 'application/json');//EVERYTHING IS AWESOME/JSON
    res.setHeader('Strict-Transport-Security', 'max-age=31540000');//HSTS

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'DELETE,*');
    res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers'] || 'x-api-key');

    //handle browser preflight (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS)
    if (req.method === 'OPTIONS' && 'access-control-request-method' in req.headers) {
        res.setHeader('Cache-Control', 'max-age: 600');
        res.end();
        return;
    }
    
    if (req.url === '/status') {
        res.end(status.getString());
    }

    //Create api.Request
    //--------------------------
    debug('[' + req_id + '] New Request');
    var request: api.Request = new api.Request(req);
    // debug('[' + req_id + '] ', request);

    if (!request.valid) {
        res.statusCode = 400;
        res.end('{"error":"Invalid request. Check to make sure you are using a real endpoint, sending the api key in the header as x-api-key, and are using the correct HTTP Method"}');
        return;
    }

    //Rate Limit!
    //--------------------------
    if (rateLimiter.shouldLimit(request)) {//if the ratelimiter thinks you should slow down
        res.statusCode = 429;
        res.end('{"error":"Rate Limited"}');
        debug('[' + req_id + '] Rate limited!');
        return; //let the ratelimiter finish the req/res and move on
    }

    //Auth Wrap!
    //--------------------------
    //wraps the code inside in authentication, and kills the connection if auth fails
    //also adds the eppn in if oauth was used
    authManager.auth(request)
        .then((request: api.Request) => {
            debug('[' + req_id + '] Authorized as "' + request.eppn + '"');
            //Process Request!
            //--------------------------
            try {
                if (request.api === 'meta') {
                    metaAPI.execute(request, res);
                } else {
                    request.execute().pipe(res);
                }

            } catch (e) {
                res.statusCode = 500;
                res.end('{"error":"Request couldn\'t be made to final api server"}');
            }

        })
        .catch((reason: string) => {
            res.statusCode = 401;//unauthorized
            res.end(jsonerror("Thrown out by the AuthManager: " + reason));
        });
}