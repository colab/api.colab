// Type definitions for path-to-regexp v1.0.3
// Project: https://github.com/pillarjs/path-to-regexp
// Definitions by: xica <https://github.com/xica>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

declare module "path-to-regexp" {
    //keys is apparently optional
    function pathToRegexp(path: string, keys?: string[], options?: pathToRegexp.Options): RegExp;
    
    module pathToRegexp {
        function parse(path : string) : Token[];
        function compile(path : string) : (tokenValues : any)=>string
        function tokensToRegExp(tokens : Token[], options : Options) : RegExp;
        function tokensToFunction(tokens : Token[]) : (tokenValues : any)=>string

        interface Token {
            name : string;
            prefix : string;
            delimiter : string; 
            optional : boolean;
            repeat : boolean;
            pattern : string;
        }
        
        interface Options {
            sensitive?: boolean;
            strict?: boolean;
            end?: boolean;
        }
    }

    export = pathToRegexp;
}
