//TS def file for swagger-tools
//written by Jason 'Toolbox' Oettinger

declare module "swagger-tools" {
	export function initializeMiddleware(swaggerObject : any, callback : (swaggerMiddleware : any) => void) : void
}