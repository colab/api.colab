"use strict";

import https = require('https');
var mysql = require('mysql');
var CONFIG = require('../config.json');
import debugInit = require('debug');
var debug = debugInit('storage.api.colab');


var esc = mysql.escape;
var pool = mysql.createPool({
  host: CONFIG.db.host,
  user: CONFIG.db.user,
  password: CONFIG.db.password,
  database: CONFIG.db.database,
  post: CONFIG.db.port
});

//Initialization

var setupQueries = [
  "CREATE TABLE `apis` (`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(64) NOT NULL, `version` VARCHAR(64) NOT NULL, `swaggerDoc` MEDIUMTEXT NOT NULL, PRIMARY KEY (`id`), UNIQUE INDEX `id_UNIQUE` (`id` ASC))",
  "CREATE TABLE `scopes` (`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(64) NOT NULL, `description` MEDIUMTEXT, PRIMARY KEY (`id`), UNIQUE INDEX `id_UNIQUE` (`id` ASC))",
  "CREATE TABLE `apps` (`id` INT NOT NULL AUTO_INCREMENT, `clientId` VARCHAR(64) NOT NULL, `appDoc` MEDIUMTEXT NOT NULL, PRIMARY KEY (`id`), UNIQUE INDEX `id_UNIQUE` (`id` ASC))",
  "CREATE TABLE `admins` (`id` INT NOT NULL AUTO_INCREMENT, `netid` VARCHAR(32) NOT NULL, PRIMARY KEY(`id`), UNIQUE INDEX `id_UNIQUE` (`id` ASC))",
];

var promises: Promise<any>[] = [];
for (var i in setupQueries) {
  promises.push(query(setupQueries[i]));
}

var initialized: Promise<any> = Promise.all(promises) // failsafe is handled in query()

export function ready() { return initialized; }

//Functions
export function getApps(): Promise<{ [index: string]: any }> {
  var qs = "SELECT * FROM apps";
  return query(qs)
    .then((result: Result) => {
      var apps: { [index: string]: any } = {};
      for (var i in result.rows) {
        try {
          var res = JSON.parse(result.rows[i].appDoc);
          apps[res.clientId] = res;
        } catch (e) {
          debug(e);
        }
      }
      return apps;
    });
}

export function addApp(a: App, token: string): Promise<App> {
  //send app to https://oauth.oit.duke.edu/oauth-registration-ws/register w/ http auth using eppn, oauthtoken
  var b: any = JSON.parse(JSON.stringify(a));
  if (b.clientSecret) { delete b.clientSecret; }
  if (b.appOwners) { delete b.appOwners; }
  if (b.expiration) { delete b.expiration; }
  debug('[OAuth] add, sending to OAuth', CONFIG.oauthRevokeAppPath, a.appOwners[0], token, JSON.stringify(b));
  return talkToOauth(CONFIG.oauthRegisterAppPath, a.appOwners[0], token, JSON.stringify(b))
    .then((c: any) => {
      debug('[OAuth] ADD get a response from OAuth', c);
      //add any missing portions to the original app
      a.clientSecret = JSON.parse(c).clientSecret;
      var qs = "INSERT INTO `apps` (`appDoc`,`clientId`) VALUES (" + esc(JSON.stringify(a)) + "," + esc(a.clientId) + ")";
      return query(qs);
    })
    .then(() => { return a; })
    .catch((e) => {
      debug('[FATAL] OAuth Error', e);
    });
}

export function updateApp(oldApp: App, newApp: App, token: string): Promise<App> {
  var b: any = JSON.parse(JSON.stringify(newApp));
  b.clientSecret = oldApp.clientSecret;
  if (b.expiration) { delete b.expiration; }
  return talkToOauth(CONFIG.oauthUpdateAppPath, oldApp.appOwners[0], token, JSON.stringify(b))
    .then((c: any) => {
      debug('[OAuth] UPDATE get a response from OAuth', c);
      newApp.clientSecret = JSON.parse(c).clientSecret;
      var qs = "UPDATE `apps` SET appDoc=" + esc(JSON.stringify(newApp)) + " WHERE clientId=" + esc(oldApp.clientId);
      return query(qs);
    })
    .then(() => {
      return newApp;
    })
}

export function removeApp(a: App, token: string): Promise<any> {
  //send app to https://oauth.oit.duke.edu/oauth-registration-ws/revoke w/ http auth using eppn, oauthtoken
  var b: any = JSON.parse(JSON.stringify(a));
  if (b.expiration) { delete b.expiration; }
  debug('[OAuth] remove, sending to OAuth', CONFIG.oauthRevokeAppPath, a.appOwners[0], token, JSON.stringify(b));
  return talkToOauth(CONFIG.oauthRevokeAppPath, a.appOwners[0], token, JSON.stringify(b))
    .then((c: any) => {
      debug('[OAuth] DELETE get a response from OAuth', c);
      var qs = "DELETE FROM `apps` WHERE clientId=" + esc(a.clientId);
      return query(qs);
    })
    .catch((e) => {
      debug('[FATAL] OAuth Error', e);
    })

}

export function getApis(): Promise<{ ids: { [name: string]: any }, swaggerDocs: { [name: string]: any } }> {
  var qs = "SELECT * FROM apis ORDER BY id DESC";
  return query(qs)
    .then((result: Result) => {
      return new Promise((resolve, reject) => {
        var swaggerDocs: { [name: string]: any } = {};
        var ids: { [name: string]: { [version: string]: number } } = {};
        for (var i in result.rows) {
          var res = result.rows[i];
          swaggerDocs[res.name] = swaggerDocs[res.name] || {};
          swaggerDocs[res.name][res.version] = JSON.parse(res.swaggerDoc);
          ids[res.name] = ids[res.name] || {};
          ids[res.name][res.version] = res.id;
        }
        resolve({ ids: ids, swaggerDocs: swaggerDocs });
      });
    });
}

export function addApi(name: string, version: string, eppn: string, token: string, swaggerDoc: any): Promise<any> {
  var promises: Promise<any>[] = [];
  if (token && 'securityDefinitions' in swaggerDoc && 'duke_auth' in swaggerDoc['securityDefinitions'] && 'scopes' in swaggerDoc['securityDefinitions']['duke_auth']) {
    var scopes = swaggerDoc['securityDefinitions']['duke_auth']['scopes'];
    debug('[add] parsed scopes', scopes);
    for (var i in scopes) {
      promises.push(addScope(i, eppn, token));
    }
  }
  var qs = "INSERT INTO `apis` (`name`,`version`,`swaggerDoc`) VALUES (" + esc(name) + "," + esc(version) + "," + esc(JSON.stringify(swaggerDoc)) + ")";
  return Promise.all(promises).then(query.bind(null, qs));
}

export function updateApi(name: string, version: string, eppn: string, token: string, swaggerDoc: any): Promise<any> {
  var promises: Promise<any>[] = [];
  if ('securityDefinitions' in swaggerDoc && 'duke_auth' in swaggerDoc['securityDefinitions'] && 'scopes' in swaggerDoc['securityDefinitions']['duke_auth']) {
    var scopes = swaggerDoc['securityDefinitions']['duke_auth']['scopes'];
    debug('[update] parsed scopes', scopes);
    for (var i in scopes) {
      promises.push(addScope(i, eppn, token));
    }
  }
  var qs = "UPDATE `apis` SET `swaggerDoc`=" + esc(swaggerDoc) + " WHERE `name`=" + esc(name) + " AND `version`=" + esc(version);
  return Promise.all(promises).then(query.bind(null, qs));
}

export function removeApi(name: string, version: string): Promise<any> {
  var qs = "DELETE FROM `apis` WHERE name=" + esc(name) + " AND version=" + esc(version);
  return query(qs);
}

export function getAdmins(): Promise<string[]> {
  var qs = "SELECT * FROM admins";
  return query(qs)
    .then((result: Result) => {
      var admins: string[] = [];
      for (var i in result.rows) {
        admins.push(result.rows[i].netid);
      }
      return admins;
    });
}

export function addAdmin(eppn: string): Promise<any> {
  var qs = "INSERT INTO `admins` (`netid`) VALUES (" + esc(eppn) + ")";
  return query(qs);
}

export function removeAdmin(eppn: string): Promise<any> {
  var qs = "DELETE FROM `admins` WHERE netid=" + esc(eppn);
  return query(qs);
}

export function getScopes(): Promise<{ [index: string]: any }> {
  var qs = "SELECT * FROM scopes";
  return query(qs)
    .then((result: Result) => {
      var scopes: { [index: string]: any } = {};
      for (var i in result.rows) {
        var storedScope = result.rows[i];
        scopes[storedScope.name] = storedScope;
      }
      return scopes;
    });
}

export function addScope(scope: string, eppn: string, token: string): Promise<any> {
  //send scopes to https://oauth.oit.duke.edu/oauth-registration-ws/addScope w/ http auth using eppn, oauthtoken
  debug('Sending scope ', scope, ' to OAuth server', eppn);
  return talkToOauth(CONFIG.oauthRegisterScopePath, eppn, token, scope)
    .then((res) => {
      debug('Heard back from OAuth server', res);
      var qs = "INSERT INTO `scopes` (`name`,`description`) VALUES (" + esc(scope) + "," + esc(scope) + ")";
      return query(qs);
    });
}

/*export function removeScope(scope: Scope): Promise<any> {
    var qs = "DELETE FROM `scopes` WHERE name=" + esc(scope.name);
    return query(qs);
}*/

function query(querystring: string): Promise<Result> {
  debug("Running query: " + querystring)
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        console.error('SQL Connection error! aborting... showing error:', err);
        process.abort();
        // reject(err);
      } else {
        connection.query(querystring, function (err, rows, fields) {
          connection.release();
          if (err && err.code !== 'ER_TABLE_EXISTS_ERROR') { debug(err); reject(err); } else { resolve({ rows: rows, fields: fields }); }
        });
      }
    });
  });
}

interface Result {
  rows: any,
  fields: any
}


//possibly NEEDS TO BE CHANGED/BUFFED/REVIEWED soon. Could provide XSS vulnerability... maybe?
var urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

interface Permission {
  service: string,
  access: string//"full" | "pseudo" | "none"
}

export class App {
  clientId: string;
  clientSecret: string;
  redirectURIs: string[];
  displayName: string;
  description: string;
  ownerDescription: string;
  privacyURL: string;
  permissions: Permission[];
  appOwners: string[];
  expiration: number;

  constructor(obj: any, eppn: string, trusted: boolean = false) {
    //trusted apps come from source or the db directly and have already been checked 
    if (trusted) {
      for (var i in obj) {
        this[i] = obj[i];
      }
      return;
    }
    this.clientId = obj.clientId;
    this.clientSecret = '';
    this.redirectURIs = obj.redirectURIs;
    this.displayName = obj.displayName;
    this.description = obj.description;
    this.ownerDescription = obj.ownerDescription;
    this.privacyURL = obj.privacyURL;
    this.permissions = obj.permissions;
    //absolutely ensure the first entry in the array is the current eppn, always. Security in authmanager (onaddapp) requires it
    this.appOwners = [eppn];
    this.expiration = Date.now() + 1000 * 60 * 60 * 24 * 365 / 2;//six months from now roughly (180 days)
  }

  public isValid(): boolean {
    try {
      var valid =
        /^[a-z0-9-]+$/i.test(this.clientId)
        && this.clientSecret.length < 100
        && typeof this.clientSecret === "string"
        && this.description.length < 10000
        && typeof this.description === "string"
        && this.ownerDescription.length < 10000
        && typeof this.ownerDescription === "string"
        && this.displayName.length < 100
        && typeof this.displayName === "string"
        && typeof this.privacyURL === "string"
        && this.privacyURL.length < 200
        && typeof this.appOwners[0] === "string";

      /*for (var i in this.redirectURIs) {
          valid = valid && urlRegex.test(this.redirectURIs[i]);
      }*/
      for (var i in this.permissions) {
        valid = valid
          && this.permissions[i].service.length < 100
          && typeof this.permissions[i].service === "string"
          && this.permissions[i].access.length < 20
          && typeof this.permissions[i].access === "string";
      }
      return valid;
    } catch (e) {
      return false;
    }
  }

  hasOwner(eppn: string): boolean {
    return this.appOwners.indexOf(eppn) > -1;
  }

}

export class Scope {
  name: string;
  description: string;

  constructor(options: { apiName?: string, name: string, description: string }) {
    if (options.apiName) {
      this.name = (options.name.indexOf(options.apiName) === 0) ? (options.name) : (options.apiName + ":" + options.name);
    }
    this.description = options.description;
  }

}

function talkToOauth(path, eppn, token, data) {
  return new Promise((resolve, reject) => {
    var authString = (new Buffer(eppn + ':' + token).toString('base64'));
    https.request({
      host: CONFIG.oauthHost,
      path: path,
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + authString
      }
    }, (res) => {
      //debug('Received ' + res.statusCode + ' from oauth server.');
      var dt = '';
      res.on('error', (err) => { reject(err); });
      res.on('data', (d) => { dt += d; });
      res.on('end', () => {
        debug('[OAuth res] dt ', dt);
        let jsonData;
        try {
          jsonData = JSON.parse(dt);
          if ('error' in jsonData) {
            reject(jsonData);
          } else {
            resolve(dt);
          }
        } catch (e) {
          debug('[OAuth res] response is not JSON', dt);
          resolve(dt);
        }
      });
    }).end(data);
  });
} 