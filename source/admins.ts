"use strict";
import storage = require('./storage');

//-----------
//STATE BELOW
//-----------
var admins: string[] = [];
//-----------
//STATE ABOVE
//-----------

update();
setInterval(update,60123);

export function update() {
    storage.getAdmins()
        .then((storedAdmins) => {admins = storedAdmins});
}

export function add(netid: string) : Promise<any> {
    return storage.addAdmin(netid).then(() => {admins.push(netid);});
}

export function remove(netid: string) : Promise<any> {
    return storage.removeAdmin(netid).then(() => {admins[admins.indexOf(netid)] = '';});
}

export function contains(netid: string) : boolean {
    return admins.indexOf(netid) > -1;
}