"use strict";
/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/request/request.d.ts"/>
import endpointManager = require('./endpointManager');
import jsonerror = require('jsonerror');
import signature = require('http-signature');
var CONFIG = require('../config.json');

import http = require('http');
import url = require('url');
import request = require('request');
import qs = require('querystring');
import debugInit = require('debug');
var debug = debugInit('request.api.colab');
import fs = require('fs');
import crypto = require('crypto');


try {
	var PRIVATE_KEY: string = fs.readFileSync(CONFIG.tls.key || "ssl/snakeoil.key", 'utf-8');
} catch (e) {
	debug("Error reading ssl cert: " + e);
}

debug('Starting module. Required all libraries.');


export class Request {
	//DATA MEMBERS BELOW
	//---------------------
	pathArray: string[];
	valid: boolean;
	user: string;
	api: string;
	version: string;
	path: string;
	method: string;
	appKey: string;
	oauthToken: string;
	bodyData: string;
	operation: any;
	req: http.IncomingMessage;
	eppn: string;
	query: string;
    queryObject: any;


	//FUNCTIONS BELOW
	//---------------------
	constructor(req: http.IncomingMessage) {
		this.req = req;
		//pull apart the request and turn it into a standard api request

		var u: url.Url = url.parse(req.url, true);
		this.bodyData = '';



		var dirtypaths = u.pathname.split('/');//split the path of access up so we can parse it
		//remove any empty paths
		this.pathArray = [];
		for (var i = 0; i < dirtypaths.length; i++) {
			if (dirtypaths[i]) { this.pathArray.push(dirtypaths[i]); }
		}


		//No more special treatment for metadocs. At least not here anyway
		if (this.pathArray.length > 1 && req.headers['x-api-key']) {
			//unnecessary due to node's lowercasing of request stuff
			this.method = req.method;//.toLowerCase();
			// we use the format /[api]/[version]/[endpoint] so things get real simple
			this.api = this.pathArray[0].toLowerCase();//first is [api]
			this.version = this.pathArray[1].toLowerCase();//second is [version]

			this.pathArray.splice(0, 2);//remove those (splice returns the elements it removed for some reason, so don't be tempted to move this into the line beneath it)
			this.path = "/" + this.pathArray.join('/');//and the remaining is the [path] (making sure to have a leading slash)
			this.query = u.search;
            this.queryObject = u.query;

			this.appKey = req.headers['x-api-key'].toLowerCase();

			this.oauthToken = req.headers['x-oauth-token'] || (req.headers['authorization'] && req.headers['authorization'].split(' ').length > 1) ? req.headers['authorization'].split(' ')[1] : '';

			this.user = req.socket.remoteAddress;//TODO make this more robust

			//Actually get the endpoint data from the endpointManager
			this.operation = endpointManager.getOperation(this);
			// debug('Operation: ', this.operation.);

			//it'll already have a scopes property on the object so this.endpoint.scopes will be an array of duke_auth scopes

			//better syntax for just double checking that the operation exists?
			this.valid = !!this.operation;
		} else {
			debug('Error parsing API Request');
			debug('pathArray length: ' + this.pathArray.length + ', x-api-key: ' + req.headers['x-api-key']);
			this.valid = false;
		}
	}
	toString(): string {
		try {
			return (this.valid ? 'API REQUEST: ' : 'INVALID API REQUEST: ') + this.user + ' => ' + this.method + " " + this.api + "/" + this.version + this.path + " apiKey: " + this.appKey;
		} catch (e) {
			return 'Error displaying request: ' + e;
		}
	}
	execute(): request.Request {
		//moved here from endpointmanager becuase it makes literally hundreds of times more sense
		//endpoint manager now does ONLY what it says, managing endpoints, and the constructor of an api.Request now pulls the operation info, scopes, etc. out of the endpoint manager in a fast way and never has to talk to the endpointManager again.



		//remap parameters (completely unnecessary actually) (maybe later)
		//not doing this

		//use swagger doc to validate
		//currently left up to the individual apis

		//Add extra data that might be helpful
		if (this.eppn) {
			this.req.headers['x-eppn'] = this.eppn;
			this.req.headers['x-netid'] = this.eppn.split('@')[0];
		}

		//sign the request
		signature.sign(this, PRIVATE_KEY);

		//remap url (just strip off api.colab.duke.edu/api/version and add the baseurl from swagger doc) (note: prevent system from redirecting to itself (for fear of infinite loops of requests))
		var url = "https://" + this.operation.api.host + this.operation.api.basePath + this.path + this.query; //path already has leading slash
		debug("Calling: " + url);
		//make the call

		//Apparently pretty critical, prevents invalid cert errors (although leaves tiny opening up for MITM attacks, the network environment is controlled enough that this is a low level TODO)
		process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
		return this.req.pipe(
			request(url, {
				headers: this.req.headers,
				method: this.method,
				timeout: 2000
			}).on('error', function(err) {
				debug(err);
				//do... nothing?
			})
		);

	}
	addToBody(data: string): void {
		this.bodyData += data;
	}
	//parseBody(): void {
	//	this.bodyData = qs.parse(this.bodyData);
	//}

	waitForBody(): Promise<any> {
		this.bodyData = '';
		var self = this;
		return new Promise(function(resolve, reject) {
			debug("Waiting for body");
			self.req.on('data', function(data) {
				debug("Body data received...");
				self.addToBody(data);
				if (self.bodyData.length > 5e7) {//~50MB
					reject(new Error("Request Body was too large! (>50MB)"));//deal with the connection outside of this, where the proper error code can be thrown.
				}
			});

			self.req.on('end', function() {
				try {
					//self.parseBody();
					debug("Body data finished... ");
					resolve({
						success: true,
						statusCode: 200,
						message: ""
					});
				} catch (e) {
					reject(new Error("Couldn't process the body data."));
				}
			});
		});
	}


}

