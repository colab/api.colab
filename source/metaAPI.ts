"use strict";
///<reference path="typings/request/request.d.ts" />

import http = require('http');
import fs = require('fs');
import debugInit = require('debug');

import storage = require('./storage');
import api = require('./api');
import endpoints = require('./endpointManager');
import authManager = require('./authManager');
import jsonerror = require('jsonerror');
import status = require('./status');

var debug = debugInit('metaAPI.api.colab');
debug('Starting module. Required all libraries.');



var metaSwag = require('../specs/metaAPI');
//add your own swagger data on server startup
endpoints.updateAPIs()
    .then(() => {
        var apiList = endpoints.getAPIList();
        debug('Checking API list', apiList);
        if (!('meta' in apiList)) {
            debug('META is not in apiList');
            storage.addApi('meta', 'v1', 'colab', '', metaSwag)
                .then((result) => {
                    debug("Added metaAPI. Result: " + JSON.stringify(result));
                })
                .then(endpoints.updateAPIs)
                .catch((err) => {
                    console.error("Failed to add metaAPI. Server probably needs a restart. " + err);
                });
        }
        debug('Found Meta. Proceed...');
    })


//here's our entry point, where requests come in if they've been determined to be metaAPI calls
export function execute(req: api.Request, res: http.ServerResponse): void {
    debug("Parsing API endpoint");
    if (req.version == 'v1') {
        switch (req.operation["x-action"]) {
            case "addAPI":
                onAPIAdd(req, res); return;
            case "removeAPI":
                onAPIRemove(req, res); return;
            case "listAPIs":
                onAPIList(req, res); return;
            case "getAPI":
                onAPIGet(req, res); return;
            case "swaggerDocs":
                onDocRequest(req, res); return;
            case "getApps":
                onAppsRequest(req, res); return;
            case "addApp":
                onAppAdd(req, res); return;
            case "removeApp":
                onAppRemove(req, res); return;
            case "getStatus":
                onStatusRequest(req, res); return;
            case "authorize":
                onAuthorize(req, res); return;
            default:
                res.statusCode = 404;
                res.end(jsonerror("endpoint not found, although you shouln't be able to even get here"));
        }
    } else {
        res.statusCode = 404;
        res.end(jsonerror("This version of the meta API does not exist"));
    }
}

function onAPIAdd(req: api.Request, res: http.ServerResponse): void {//adds and updates apis
    //extract required params for endpointManager.addAPI and run it
    req.waitForBody()
        .then(function() {
            debug("Adding API to manager");
            return endpoints.addOrUpdateAPI(req.eppn, req.oauthToken, JSON.parse(req.bodyData));
        }).then(function(result) {
            res.statusCode = result.statusCode;
            res.end('{}');
        }).catch(metaAPICatcher.bind(null, res));
}

function onAPIRemove(req: api.Request, res: http.ServerResponse): void {//removes apis
    //extract required params for endpointManager.removeAPI and run it
    req.waitForBody()
        .then(() => {
            debug("Removing api from manager");
            return endpoints.removeAPI(req.eppn, req.oauthToken, JSON.parse(req.bodyData));
        })
        .then((result) => {
            res.statusCode = result.statusCode;
            res.end('{}');
        }).catch(metaAPICatcher.bind(null,res));
}

function onAPIList(req: api.Request, res: http.ServerResponse): void {
    //send the response generated by endpointManager.getAPIList
    debug("Listing APIs");
    try {
        res.end(JSON.stringify(endpoints.getAPIList()));
    } catch (e) {
        metaAPICatcher(res, {
            statusCode: 500,
            message: "Server Error"
        });
    }
}

function onAPIGet(req: api.Request, res: http.ServerResponse): void {
    debug("Returning swagger doc for api");
    try {
        res.end(JSON.stringify(endpoints.getAPIDoc(req.pathArray[1], req.pathArray[2])));
    } catch (e) {
        metaAPICatcher(res, {
            statusCode: 500,
            message: "Server Error"
        });
    }
}

function onDocRequest(req: api.Request, res: http.ServerResponse): void {
    debug("Returning swagger doc for all apis");
    try {
        res.end(JSON.stringify(endpoints.getFullDoc()));
    } catch (e) {
        metaAPICatcher(res, {
            statusCode: 500,
            message: "Server Error"
        });
    }
}

function onAppAdd(request: api.Request, res: http.ServerResponse): void {//deals with additions of apps and modifications of apps (and permissions)
    //verify app structure
    request.waitForBody()
        .then(function() {
            return request;
        })
        .then(authManager.onAddApp)
        .then((app: storage.App) => {
            res.end(JSON.stringify(app));
        })
        .catch(metaAPICatcher.bind(null, res));

}

function onAppsRequest(request: api.Request, res: http.ServerResponse): void {//removes apps
    try {
        if (request.queryObject.all && request.queryObject.all !== 'false' && request.queryObject.all !== '0') {
            res.end(JSON.stringify(authManager.viewAppsAs(request.eppn)));
        } else {
            res.end(JSON.stringify(authManager.viewMyApps(request.eppn)));
        }
    } catch (e) {
        metaAPICatcher(res, "Couldn't build app list.");
    }
}

function onAppRemove(request: api.Request, res: http.ServerResponse): void {//removes apps
    request.waitForBody()
        .then(() => {
            return request;
        })
        .then(authManager.onRemoveApp)
        .then(() => {
            res.end("{}");
        })
        .catch(metaAPICatcher.bind(null, res));
}

function onAuthorize(request: api.Request, res: http.ServerResponse): void {
    var options = {
        hostname: 'localhost',
        port: 80,
        path: '/authorize.php',
        method: 'GET'
    };

    var proxy = http.request(options, (proxyResponse) => {
        proxyResponse.pipe(res, {
            end: true
        });
    });

    request.req.pipe(proxy, {
        end: true
    });
}

function onStatusRequest(request: api.Request, res: http.ServerResponse): void {
    res.end(status.getString);
}

function metaAPICatcher(res: http.ServerResponse, err: any): void {
    debug("Caught error: ");
    debug(err);
    if (typeof err === 'object' && "statusCode" in err) {
        res.statusCode = err.statusCode;
    } else {
        res.statusCode = 500;
    }
    if (typeof err === 'object' && "message" in err) {
        res.end(jsonerror(err.message));
    } else {
        res.end(jsonerror(err));
    }
}

export interface Scope {
    name: string
    description: string
}

interface API { any }; //Use the swaggerDoc data