"use strict";
/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/swagger-tools/swagger-tools.d.ts"/>
/// <reference path="typings/request/request.d.ts"/>
/// <reference path="typings/path-to-regexp/path-to-regexp.d.ts"/>

import authManager = require('./authManager');
import rateLimiter = require('./rateLimiter');
import admins = require('./admins');
var CONFIG = require('../config.json');
import storage = require('./storage');
import api = require('./api');

import crypto = require('crypto');
import http = require('http');
var sway = require('sway');
import request = require('request');
import debugInit = require('debug');
var debug = debugInit('endpointManager.api.colab');


debug('Starting module. Required all libraries.');


//-----------
//STATE BELOW
//-----------
var swaggerDocs: { [name: string]: any } = {};//make original swagger documents array
var swaggers: { [name: string]: any } = {};//make swaggerAPI Objects array
//-----------
//STATE ABOVE
//-----------

updateAPIs();
setInterval(updateAPIs, 45787); //approximately every 45 seconds


export function updateAPIs() {
  return storage.ready()
    .then(() => storage.getApis())
    .then((res) => {

      debug('SwaggerDocs Loaded: ' + Object.keys(swaggerDocs).length);

      for (var apiName in res.swaggerDocs) {
        for (var ver in res.swaggerDocs[apiName]) {
          //if this api doesn't exist yet, or it's got a higher id (indicating it's been updated)
          if (!(swaggerDocs[apiName] && swaggerDocs[apiName][ver]) || res.ids[apiName][ver] > res.ids[apiName][ver]) {
            sway.create({ 'definition': res.swaggerDocs[apiName][ver] })
              .then(function (name, v, s) {
                debug("Reconstituting " + name + " version:" + v);

                if (!(name in swaggers)) { swaggers[name] = {}; }
                swaggers[name][v] = s;
                rateLimiter.onAddApi(s);
              }.bind(null, apiName, ver))
              .catch(function (e) { debug(e); });
          }

        }
      }
      swaggerDocs = res.swaggerDocs;

    });

}

export function addOrUpdateAPI(eppn: string, token: string, swaggerObject: any): Promise<Result> {
  debug("Validating API...");

  //double check that .info exists and then add this user to x-owners if it doesnt exist
  if (!('info' in swaggerObject)) {
    debug("No info in swagger object!");
    return Promise.reject<Result>({
      statusCode: 400,
      message: "Swagger syntax didn't validate (try making it with a swagger tool). It's missing the 'info' section."
    });
  }

  //confirm that the api name and version are acceptible
  var apiName: string = swaggerObject.info['x-name'] || swaggerObject.info.title || "";
  apiName = apiName.toLowerCase();
  var version: string = swaggerObject.info.version || "";
  version = version.toLowerCase();


  var pathExp = /^[\w\-.]+$/;//allow alphanumeric, underscores, and dashes
  if (!apiName || !version || !pathExp.test(apiName) || !pathExp.test(version) || apiName.length > 20 || version.length > 8) {
    return Promise.reject<Result>({
      statusCode: 400,
      message: "Invalid version number or API name (may be too long, or have whitespace). Use /info/x-name to set a machine name that's different from the title."
    });
  }

  //if it already exists, validate authorization to update
  if (apiName in swaggerDocs && !admins.contains(eppn)) {
    //check every version
    for (var v in swaggerDocs[apiName]) {
      //ensure the eppn is one of x-owners
      if (swaggerDocs[apiName][v].info['x-owners'].indexOf(eppn) < 0) {
        debug("No permission to edit this object!");
        return Promise.reject<Result>({
          statusCode: 401,
          message: 'You do not have the authority to add/update this api'
        });
      }
    }
  }

  //Force ownership for now...
  //if (!('x-owners' in swaggerObject['info']) || typeof swaggerObject['info']['x-owners'] !== 'array') {//add this netid to x-owners if it isn't there already 
  swaggerObject['info']['x-owners'] = [eppn];
  /*} else {
      if (swaggerObject['info']['x-owners'].indexOf(eppn) < 0) {
          swaggerObject
      }
  }*/


  //All apis are unofficial unless marked specifically so in the db
  swaggerObject['info']['x-official'] = false;
  debug("API changes are authorized...");

  var isUpdate;

  return sway.create({ 'definition': swaggerObject })
    .then(function (s): Promise<Result> {
      //validate against swagger schema
      if (s.validate().errors.length !== 0) {
        return Promise.reject<Result>({
          statusCode: 400,//BAD REQUEST
          message: "Swagger syntax didn't validate (try making it with a swagger tool like dukeswag)."
        });
      }

      var isUpdate = apiName in swaggerDocs && version in swaggerDocs[apiName];

      if (!(apiName in swaggers)) { swaggers[apiName] = {}; }//ensure the api exists
      swaggers[apiName][version] = s;//add api to swaggers

      if (!(apiName in swaggerDocs)) { swaggerDocs[apiName] = {}; }//ensure the api exists
      swaggerDocs[apiName][version] = swaggerObject;//add to swaggerdocs

      return s;
    })
    .then(authManager.onAddApi)
    .then(rateLimiter.onAddApi)
    .then(() => {
      if (!isUpdate) {//if it doesn't exist yet
        storage.addApi(apiName, version, eppn, token, swaggerObject) //store swaggerObject
      } else {
        storage.updateApi(apiName, version, eppn, token, swaggerObject);
      }
    })
    .then(() => {
      return Promise.resolve({
        statusCode: 200,
        message: ""
      });
    })
    .catch((err) => {
      debug(err);
      return Promise.reject({
        statusCode: 500,
        message: "Server Error"
      });
    });
}

export function removeAPI(eppn: string, token: string, swaggerObject: any): Promise<Result> {
  debug("Validating API...");

  //double check that .info exists and then add this user to x-owners if it doesnt exist
  if (!('info' in swaggerObject)) {
    debug("No info in swagger object!");
    return Promise.reject<Result>({
      statusCode: 400,
      message: "Swagger syntax didn't validate (try making it with a swagger tool). It's missing the 'info' section."
    });
  }

  //confirm that the api name and version are acceptible
  var apiName: string = swaggerObject.info['x-name'] || swaggerObject.info.title || "";
  apiName = apiName.toLowerCase();
  var version: string = swaggerObject.info.version || "";
  version = version.toLowerCase();

  try {
    if (!swaggerDocs[apiName] || !swaggerDocs[apiName][version]) {
      return Promise.reject<Result>({
        statusCode: 404,
        message: "That is not an API that exists..."
      });
    }
    if (swaggers[apiName][version].info['x-owners'].indexOf(eppn) < 0 && !admins.contains(eppn)) {
      return Promise.reject<Result>({
        statusCode: 401,
        message: "UNAUTHORIZEDDDD"
      });
    }

    rateLimiter.onRemoveApi(swaggers[apiName][version]);

    //remove from the list of swaggers
    if (swaggerDocs[apiName]) {
      delete swaggerDocs[apiName][version];
      delete swaggers[apiName][version];
    }

    return storage.removeApi(apiName, version)
      .then((success): Promise<Result> => {//success
        return Promise.resolve({
          statusCode: 200,
          message: ""
        });
      })
      .catch((err): Promise<void> => {//failure
        return Promise.reject({
          statusCode: 500,
          message: "Had trouble saving. Things could be weird now."
        });
      });


  } catch (err) {
    debug(err);
    return Promise.reject<Result>({
      statusCode: 500,
      message: "Failed in removeAPI"
    });
  }
}

//synchronous
export function getAPIList(): any {
  var apis = {};
  for (var api in swaggers) {//iterate through all the APIs
    var versions = [];
    for (var v in swaggers[api]) {//iterate through all versions
      versions.push(v);
    }
    apis[api] = versions;//store a list of all versions inside of a list of all apis
  }
  return apis;
}

//synchronous
export function getAPIDoc(name: string, version: string): any {
  //need to translate some parts of it
  debug("Returning swaggerDocs[" + name + "][" + version + "]");
  if (name in swaggerDocs && version in swaggerDocs[name]) {
    //TODO Optimize
    //deep copy
    var swagDoc = JSON.parse(JSON.stringify(swaggerDocs[name][version]));
		/*var swagDoc = {};
		var keys = Object.keys(swaggerDocs[name][version]);
		var i = keys.length;
		while (i--) {
			swagDoc[keys[i]] = swaggerDocs[name][version][keys[i]];
		}*/
    //CHANGE THE BASEPATH FROM WHATEVER THE INTERNAL BASE PATH IS TO THE {API}/{VERSION} SETUP FOR CONSUMPTION BY CLIENTS
    swagDoc['basePath'] = "/" + name + "/" + version;
    //CHANGE THE HOST. DUH.
    swagDoc['host'] = "api.colab.duke.edu";
    //FORCE HTTPS SCHEME
    swagDoc['schemes'] = ["https"];
    //IS THAT IT?
    //NOPE
    //FORCE SECURITY DEFINITIONS
    //make sure secdef exists
    if (!('securityDefinitions' in swagDoc)) {
      swagDoc['securityDefinitions'] = {};
    }
    swagDoc['securityDefinitions']['api_key'] = {
      "type": "apiKey",
      "name": "api_key",
      "in": "header"
    };
    if ('duke_auth' in swagDoc['securityDefinitions']) {
      swagDoc['securityDefinitions']['duke_auth']['type'] = "oauth2";
      swagDoc['securityDefinitions']['duke_auth']['authorizationUrl'] = "https://" + CONFIG.oauthHost + CONFIG.oauthAuthorizationPath;
      swagDoc['securityDefinitions']['duke_auth']['flow'] = "implicit";
    } else {
      swagDoc['securityDefinitions']['duke_auth'] = {
        "type": "oauth2",
        "authorizationUrl": "https://" + CONFIG.oauthHost + CONFIG.oauthAuthorizationPath,
        "flow": "implicit",
        "scopes": {
          "meta:api:write": "Modify your APIs"
        }
      };
    }

    //FORCE x-api-key HEADER REQUIREMENT (IN SECURITY, NOT PER OPERATION)
    if (!('security' in swagDoc)) {
      swagDoc['security'] = [{ "api_key": [] }];
    } else {
      //assumed array because otherwise it wouldn't have passed swagger validation
      var needToAdd = true;
      for (var j in swagDoc['security']) {
        if ('api_key' in swagDoc['security'][j]) {
          needToAdd = false;
          swagDoc['security'][j]['api_key'] = [];//just to be absolutely sure
        }
      }
      if (needToAdd) { swagDoc['security'].push({ "api_key": [] }); }
    }

    //HIDE x-netid HEADER REQUIREMENTS FROM CLIENTS
    for (var j in swagDoc['paths']) {
      for (var k in swagDoc['paths'][j]) {
        var op = swagDoc['paths'][j][k];
        if (typeof op === 'object') {
          if (op['parameters']) {
            for (var p = 0; p < op['parameters'].length; p++) {
              //name assumable
              if (op['parameters'][p].name === "x-netid" || op['parameters'][p].name === "x-eppn") {
                op['parameters'].splice(p, 1);
              }
            }
          }
          if (op['security']) {
            var exists = false;
            for (var p = 0; p < op['security']; p++) {
              if ('api_key' in op['security'][p]) {
                exists = true;
              }
            }
            if (!exists) {
              op['security'].push({ "api_key": [] });
            }
          }
        }
      }
    }
    return swagDoc;
  } else {
    return {};
  }
}

export function getFullDoc(): any {
  debug("Returning all swaggerDocs");

  var swagDoc = {};

  for (var s in swaggerDocs) {
    swagDoc[s] = {};
    for (var v in swaggerDocs[s]) {
      swagDoc[s][v] = getAPIDoc(s, v);
    }
  }

  return swagDoc;
}

//returns an endpoint object (at swaggers[api][version].resolved.paths[method])
//returns undefined on failure to acquire for any reason
export function getOperation(req: api.Request): any {
  if (!swaggers[req.api] || !swaggers[req.api][req.version]) { return undefined; }
  var api = swaggers[req.api][req.version];
  //Remember, this is the base path of the backend api, not the client-facing basepath! It's needed to make getOperation function appropriately. Additionally, the getOperation function is being used as though an IncomingMessage were being passed to it, rather than url/method strings, in order to get regex evaluation!
  var op: any = api.getOperation({
    url: ((!('basePath' in api) || api.basePath === '/') ? '' : api.basePath) + req.path,
    method: req.method
  });
  if (op) {
    op.scopes = [];
    if ('security' in op) {
      for (var i in op.security) {
        if ('duke_auth' in op.security[i]) {
          op.scopes = op.security[i]['duke_auth'];
        }
      }
    }
  }

  return op;
}

//Promise result info
interface Result {
  success: boolean;
  statusCode: number;//suggested status code
  message: string;
}