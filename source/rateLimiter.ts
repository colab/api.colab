"use strict";
/// <reference path="typings/node/node.d.ts"/>


//Rate limiter used in api.colab 
//Originally developed by Jason "Toolbox" Oettinger

import http = require('http');
import https = require('https');
import api = require('./api');
import debugInit = require('debug');
var CONFIG = require('../config.json');
import jsonerror = require('jsonerror');
	var debug = debugInit('rateLimiter.api.colab');

debug('Starting module. Required all libraries.');



//the overall idea is simple. feed it a request and a scheme. Figure out if the request should continue or be limited.
//SIMPLE, HE SAYS.
//Also, we should consider rate limiting at the endpoint level rather than the api level. just a thought.
//TODO IMPROVE design for multiple simultaneous rateLimiters running without inter-communication (to allow distributed upscaling of the server)

//-----------------------------------------------------------------------------------------------------------------------
//------set up buckets---------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------


export class BucketSet {//class to hold the buckets and set their values
	constructor() {//initialize clearing of buckets; (trying to) minimize the actual callbacks made 
		var oneSecond = 1000;//milliseconds
		var oneMinute = 1000*60;
		var oneHour   = 1000*60*60;
		var oneDay    = 1000*60*60*24;		
	}
	increment(api : string, user : string, appkey : string) {//unrolled some code that could be in multiple functions for speed
		//create bucket at each timescale when a new api is referenced
        if (!this.sec) {
            this.sec = [];
            this.min = [];
            this.hour = [];
            this.day = [];
        }
		if (!this.sec[api]) {
			this.sec[api]  = new Bucket;
			this.min[api]  = new Bucket;
			this.hour[api] = new Bucket;
			this.day[api]  = new Bucket;
			this.lastSec = new Date();
			this.lastMin = new Date();
			this.lastHour = new Date();
			this.lastDay = new Date();
		}
		//Check whether we should reset the buckets
		if (Date.now() > this.lastSec.getTime() + 1000) {
			delete this.sec;
			this.sec = [];
			this.sec[api] = new Bucket;
			this.lastSec = new Date();
			if (Date.now() > this.lastMin.getTime() + 60000) {
				delete this.min;
				this.min = [];
				this.min[api] = new Bucket;
				this.lastMin = new Date();
				if (Date.now() > this.lastHour.getTime() + 3600000) {
					delete this.hour;
					this.hour = [];
					this.hour[api] = new Bucket;
					this.lastHour = new Date();
					if (Date.now() > this.lastDay.getTime() + 86400000) {
						delete this.day;
						this.day = [];
						this.day[api] = new Bucket;
						this.lastDay = new Date();
					}
				}
			}
		}
		
		//increment the buckets at each timescale for that api
		this.sec[ api].increment(user,appkey);
		this.min[ api].increment(user,appkey);
		this.hour[api].increment(user,appkey);
		this.day[ api].increment(user,appkey);
		
		
	}
	//TODO add analytics storage options to clear___ functions
	clearSec() {
		this.sec = [];
	}
	clearMin() {
		this.min = [];
	}
	clearHour() {
		this.hour = [];
	}
	clearDay() {
		this.day = [];
	}
	sec:  Array<Bucket>;
	min:  Array<Bucket>;
	hour: Array<Bucket>;
	day:  Array<Bucket>;
	lastSec:  Date;
	lastMin:  Date;
	lastHour: Date;
	lastDay:  Date;
}

export class Bucket { //class to hold and clear data about user, app, and total usage per timescale per api
	constructor() {
		this.clear();
	}
	clear() {
		this.users = [];
		this.apps  = [];
		this.total = 0;
	}
	increment(user : string, app : string): void {
		if (!this.users[user]) {
			this.users[user] = 0;
		}
		if (!this.apps[app]) {
			this.apps[app] = 0;
		}
		
		this.users[user]++;
		this.apps[app]++;
		this.total++;
	}
	users  : Array<number>;
	apps   : Array<number>;
	total : number;
}

var buckets : BucketSet = new BucketSet();

//-----------------------------------------------------------------------------------------------------------------------
//------set up schemes---------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
var schemes : RateLimitingScheme[][] = [];
var defaultScheme : RateLimitingScheme = {
	perSec: 50,
	perMin: 300,
	perHour: 2000,
	perDay: 40000
};
//schemes are stored per api per Appkey/User. User schemes override appkey schemes which override api keys.
//The schemes data structure probably needs a bit of explanation The idea is to maximize flexibility of control schemes while making the data storage implications and processing as simple as possible. For this reason, each application can have several limiting schemes, all eventually falling back onto the default as defined in defaultScheme. Firstly, every api has a global limit (defaulting to defaultScheme) to give a level of overall protection to the system. This limit is likely not sufficient for most cases but nevertheless is the default. It's defined as schemes[api]['_total']. This is checked first to ensure that the api system cannot trash critical systems, no matter how high demand is. If usage is below the threshold, then appkey-level limits are checked. First it's checked whether there is a specific rule for this appkey (schemes[api][appkey]), and if not, the default value of schemes[api]['_perapp'] is used. Then finally a user-level check is done, similarly first looking for schemes[api][user], and using schemes[api]['_peruser'] as a fallback. The namespace of _user_ and _appkey_ should have no overlap (and also should have no overlap with the phrases 'default', 'peruser', and 'perapp'), so the flat data structure works well.

//used internally to simplify adding of schemes according to the data structure of _schemes_
function addScheme(scheme : RateLimitingScheme, api : string, key? : string) : void {//function only to be used internally, not exported
	if (!schemes[api]) {
		schemes[api] = [];
		schemes[api]['_total'] = defaultScheme; //make sure at least the three defaults exist (simplifies (and speeds up) code in shouldLimit)
		schemes[api]['_perApp'] = defaultScheme;
		schemes[api]['_perUser'] = defaultScheme;
		if (!key) {
			schemes[api][key] = scheme;
		}
	} else {
            if (key === "_total" || key === "_perApp") {
                //divide total and app into each cluster
                for (var i in scheme) {
                    if (typeof scheme[i] === 'number') {
                        scheme[i] /= CONFIG.clusterSize || 1;
                    }
                }
            }
			schemes[api][key] = scheme;
	}
}

//set an exception (for a user or for an app) to the rules applying to an api
//USE THIS!
export function addException(scheme : RateLimitingScheme, api : string, userOrAppKey : string) : void {
	addScheme(scheme,api,userOrAppKey);
}

//Set a scheme that applies to an entire api
//USE THIS!
export function setScheme(scheme : RateLimitingScheme, api : string, perWhat? : string) : void{
	if (perWhat === 'user' || perWhat === 'perUser') {
		addScheme(scheme,api,'_perUser');
	} else if (perWhat === 'app' || perWhat === 'appkey' || perWhat === 'perApp') {
		addScheme(scheme,api,'_perApp');
	} else  if (perWhat === 'total' || perWhat === 'perTotal') {
		addScheme(scheme,api,'_total');
	}
}

//export interface RateLimitingScheme {
//	perUser : bucketLimits;
//	perApp : bucketLimits;
//	penalty : string;
//}

interface RateLimitingScheme {
	perSec : number;
	perMin? : number;
	perHour? : number;
	perDay? : number;
	penalty? : string;
}

  
	
//-----------------------------------------------------------------------------------------------------------------------
//------Set up limiting functions----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------	
	
export function shouldLimit(request : api.Request) : boolean {
	//increment user, appkey, and total in their respective buckets
	buckets.increment(request.api,request.user,request.appKey);
	
	//evaluate against relevent schemes, buckets should have been guarunteed to be created
	var sec : Bucket = buckets.sec[request.api];
	var min : Bucket = buckets.min[request.api];
	var hour: Bucket =buckets.hour[request.api];
	var day : Bucket = buckets.day[request.api];
	
		//check api total bucket, using the default scheme if none has been set
		if (typeof schemes[request.api] === 'undefined') {//THIS API HAS NO SCHEME DATA SET
			//if (sec.total > defaultScheme.perSec || min.total > defaultScheme.perMin || hour.total > defaultScheme.perHour || day.total > defaultScheme.perDay) {return true;}
			//Switching to always fail if api has no rate limit info yet
			return false;
		} else {//pull down the scheme data and compare
			var _total : RateLimitingScheme = schemes[request.api]['_total'] || defaultScheme;
			if (sec.total > _total.perSec || min.total > _total.perMin || hour.total > _total.perHour || day.total > _total.perDay) {return true;}
		}
		//check if app has an exception
		var _perApp : RateLimitingScheme = schemes[request.api]['_perApp'] || defaultScheme;
		if (schemes[request.api][request.appKey]) {_perApp = schemes[request.api][request.appKey];}
			//check app bucket
			if (sec.total > _perApp.perSec || min.total > _perApp.perMin || hour.total > _perApp.perHour || day.total > _perApp.perDay) {return true;}
			
		//check if user has an exception
		var _perUser : RateLimitingScheme = schemes[request.api]['_perUser'] || defaultScheme;
		if (schemes[request.api][request.user]) {_perUser = schemes[request.api][request.user];}
			//check user bucket
			if (sec.total > _perUser.perSec || min.total > _perUser.perMin || hour.total > _perUser.perHour || day.total > _perUser.perDay) {return true;}
	
	//return false (we should not limit) if we've made it here and not been restricted
	return false;
}

//-----------------------------------------------------------------------------------------------------------------------
//------Setup functions--------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
export function onAddApi(s) : boolean {
	var name = s.info['x-name'] || s.info.title || "ERROR";
	addScheme(defaultScheme,name);
	if ('x-rate-limit' in s) {
		for (var perWhat in s['x-rate-limit']) {
			debug("Setting rate limit for " + name + ": " + perWhat);
			setScheme(s['x-rate-limit'][perWhat],name,perWhat);
		}
	}
	return true;
}
export function onRemoveApi(s) : boolean {
	var name = s.info['x-name'] || s.info.title || "ERROR";
	delete schemes[name];
	return true;
}