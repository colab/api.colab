describe('api.ts',function() {
   try {var api = require('../compiled/api');} catch(e) {}
   it('should exist', function() {
       require('../compiled/api');
   });
   
   describe('Request', function() {
        it('should mark keyless requests as invalid');
        it('should mark incorrectly formed paths as invalid');
        it('should extract all expected data from the http request');
        
        describe('.toString', function() {
            it('should correctly print a summary of the request');
        });
        
        describe('.waitForBody', function() {
            it('should wait until the request body has been transmitted');
            it('should reject bodies that are too large');
        });
        
        describe('.execute', function() {
            it('should correctly pass the request along to the target');
        });
   });

});