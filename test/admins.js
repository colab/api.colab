
describe('admins.ts', function() {
    try {var admins = require('../compiled/admins');} catch(e) {}
    it('should exist', function() {
        require('../compiled/admins');
    });
    
    describe('.add()', function() {
        it('should allow admins to be added', function() {
            admins.add('netid');
        });
    });
    describe('.remove()', function() {
        it('should allow admins to be removed', function() {
            admins.remove('netid');
        });
    });
    describe('.update()', function() {
        it('should see changes when admins are added and removed', function() {
            
        });
    });
    
});