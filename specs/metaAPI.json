{
    "swagger": "2.0",
    "info": {
        "version": "v1",
        "title": "Co-Lab MetaAPI",
        "x-name": "meta"
    },
    "host": "api.colab.duke.edu",
    "basePath": "/meta",
    "schemes": [
        "https"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "x-rate-limit": {
        "perUser": {
            "perSec": 100,
            "perMin": 1000,
            "perHour": 100000,
            "perDay": 1000000
        },
        "perApp": {
            "perSec": 200,
            "perMin": 2500,
            "perHour": 100000,
            "perDay": 1000000
        },
        "total": {
            "perSec": 1000,
            "perMin": 60000,
            "perHour": 10000000,
            "perDay": 100000000
        }
    },
    "securityDefinitions": {
        "api_key": {
            "type": "apiKey",
            "name": "x-api-key",
            "in": "header"
        },
        "duke_auth": {
            "type": "oauth2",
            "authorizationUrl": "https://oauth.oit.duke.edu/oauth/authorize.php",
            "flow": "implicit",
            "scopes": {
                "meta:api:write": "Modify your APIs",
                "meta:apps:write": "Modify/create registered apps",
                "meta:apps:read": "See the apps you have registered",
                "oauth_registrations": "Make modifications to the oauth-enabled systems"
            }
        }
    },
    "security": [
        {
            "api_key": []
        }
    ],
    "paths": {
        "/docs": {
            "get": {
                "description": "Returns the compiled swaggerdocs",
                "responses": {
                    "200": {
                        "description": "Success"
                    }
                },
                "x-action": "swaggerDocs"
            }
        },
        "/apis": {
            "get": {
                "description": "Returns a list of all of the apis and their versions.\n",
                "responses": {
                    "200": {
                        "description": "Successful response"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    }
                ],
                "x-action": "listAPIs"
            },
            "post": {
                "description": "Updates/creates an api with a name and version specified in the included swagger document",
                "parameters": [
                    {
                        "name": "swagger",
                        "in": "body",
                        "description": "The swagger document describing the API to be syndicated, from the point of view of the api.colab server",
                        "schema": {
                            "type": "object"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful update/creation"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    },
                    {
                        "duke_auth": [
                            "oauth_registrations",
                            "meta:api:write"
                        ]
                    }
                ],
                "x-action": "addAPI"
            },
            "delete": {
                "description": "Deletes an api with a name and version specified in the included swagger document",
                "parameters": [
                    {
                        "name": "swagger",
                        "in": "body",
                        "description": "The swagger document describing the API to be syndicated, from the point of view of the api.colab server",
                        "schema": {
                            "type": "object"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful removal"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    },
                    {
                        "duke_auth": [
                            "oauth_registrations",
                            "meta:api:write"
                        ]
                    }
                ],
                "x-action": "removeAPI"
            }
        },
        "/apis/{api}/{version}": {
            "get": {
                "description": "Returns the swagger document for a particular api",
                "parameters": [
                    {
                        "name": "api",
                        "in": "path",
                        "description": "The api name that you wish to access.",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "version",
                        "in": "path",
                        "description": "Specifies the version desired. Defaults to current version.",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful query"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    }
                ],
                "x-action": "getAPI"
            }
        },
        "/apps": {
            "get": {
                "description": "Returns a list of all registered apps (defaults to only showing yours)",
                "parameters": [
                    {
                        "name": "all",
                        "in": "query",
                        "description": "Whether or not to display all apps or only the ones you own. Defaults to false.",
                        "type": "boolean",
                        "required": false,
                        "default": false
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful query"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    },
                    {
                        "duke_auth": [
                            "oauth_registrations",
                            "meta:apps:read"
                        ]
                    }
                ],
                "x-action": "getApps"
            },
            "post": {
                "description": "Adds/Updates an application object (and its permissions)",
                "parameters": [
                    {
                        "in": "body",
                        "name": "App",
                        "description": "Actual App Metadata object to be registered, with the requested permissions",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful update of an app"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    },
                    {
                        "duke_auth": [
                            "oauth_registrations",
                            "meta:apps:write"
                        ]
                    }
                ],
                "x-action": "addApp"
            },
            "delete": {
                "description": "Deletes an application object (and its permissions)",
                "parameters": [
                    {
                        "in": "body",
                        "name": "App",
                        "description": "The App object to be deleted",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "successful deletion"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    },
                    {
                        "duke_auth": [
                            "oauth_registrations",
                            "meta:apps:write"
                        ]
                    }
                ],
                "x-action": "removeApp"
            }
        },
        "/status": {
            "get": {
                "description": "Returns current status of the api system",
                "responses": {
                    "200": {
                        "description": "successful query"
                    }
                },
                "security": [
                    {
                        "api_key": []
                    }
                ],
                "x-action": "getStatus"
            }
        }
    }
}